function logout() {
    localStorage.clear();
    window.location.assign("index.html");
}

$(document).ready(function() {
    var token = localStorage.getItem("token");
    var name = localStorage.getItem("username");
    if (token != "" && token != "undefined" && token != null){
        document.getElementById("login_a").style.display = "none";
        document.getElementById("account_a").innerHTML = "Welcome " + name;
        document.getElementById("logout_a").style.display = "block";
    } else{
        document.getElementById("login_a").style.display = "block";
        document.getElementById("account_a").style.display = "none";
        document.getElementById("logout_a").style.display = "none";
    }
        
});

