$(document).on("submit", "#loginForm", btnLoginSubmit);
function btnLoginSubmit(e) {
    e.preventDefault();

    const email = document.getElementById("txtUsername").value;
    const pass = document.getElementById("txtPassword").value;
    const clientID = 2;
    const grant = "password";
    var csecret = localStorage.getItem("client_secret");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "POST",
        url: host_url +"/oauth/token",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Type': 'application/json',
        },
        data: {
            username: email,
            password: pass,
            client_id: clientID,
            client_secret: csecret,
            grant_type: grant,
        }
    })
        .then(function (res) {
            localStorage.setItem("token", res.data.access_token);
            axios({
                method: "GET",
                url: host_url +"/user/profile?include=roles",
                headers: {
                    'Authorization': "Bearer " + res.data.access_token,
                    'Accept': 'application/json'
                }
            })
                .then(function (reschild) {
                    var name = reschild.data.data.name;
                    localStorage.setItem("username", name);
                    window.location.assign("index.html");
                })
                .catch(function (error) {
                    alert("Error: " + error)
                    console.log(error);
                    location.reload();
                });
        })
        .catch(function (error) {
            alert("Wrong Email or Password.")
            console.log(error);
            location.reload();
        });

    $("#loginForm").validate(
        { submitHandler: function(form) {} }
    );
}

$(document).ready(function() {
    const host_url = $.getJSON( "../js/controller/config.json", function( json ) {
        localStorage.setItem("host_url", json.server_host);
    });
    const client_secret = $.getJSON( "js/controller/config.json", function( json ) {
        localStorage.setItem("client_secret", json.client_secret);
    });
});
