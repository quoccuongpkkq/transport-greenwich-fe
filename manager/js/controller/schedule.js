$(document).on("submit", "#createForm", btnCreateSubmit);
function btnCreateSubmit(e) {
    e.preventDefault();
    const boat = document.getElementById("sBoat").value;
    const departuretime  = document.getElementsByClassName("departuretime")
    const arrivaltime = document.getElementsByClassName("arrivaltime");
    var timevals = "";
    for(var i=0; departuretime[i]; ++i){
        timevals = timevals + departuretime[i].value + '-' + arrivaltime[i].value + ',';        
    }
    timevals = timevals.substring(0, timevals.length - 1);
    const departure  = document.getElementById("sDeparture").value;
    const arrival = document.getElementById("sArrival").value;
    var dow = ""; 
    var inputElements = document.getElementsByClassName('checkbox-dow');
    for(var i=0; inputElements[i]; ++i){
        if(inputElements[i].checked){
            dow = dow + inputElements[i].value + ',';
        }
    }
    if (dow.length > 0){
        dow = dow.substring(0, dow.length - 1);
    } else{
        alert("Please choose days of week.");
        return;
    }
    const des  = document.getElementById("txtDescription").value;
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "POST",
        url: host_url +"/schedule/create_schedule",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            boat_id: boat,
            timevals: timevals,
            departure_station_id: departure,
            arrival_station_id: arrival,
            days_of_week: dow,
            description: des
        }
    })
        .then(function (res) {
            alert("Create successfully.")
            location.reload();
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

    $("#createForm").validate(
        { submitHandler: function(form) {} }
    );
}

function changeActive(obj, id) {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "PUT",
        url: host_url +"/schedule/update_schedule",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            id: id,
            active: obj.checked
        }
    })
        .then(function (res) {
            location.reload();
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });
}

function setEditData(id) {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url +"/schedule/find_schedule",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        params: {
            id: id
        }
    })
        .then(function (res) {
            if (res.data.length > 0){
                document.getElementById("sEditBoat").value = res.data[0].boat_id;
                // Set time interval group 
                var timevals = res.data[0].timevals.split(",");
                var departuretime1 = document.getElementsByClassName("editdeparturetime");
                var arrivaltime1 = document.getElementsByClassName("editarrivaltime");
                var gettime = timevals[0].split("-");
                departuretime1[0].value = gettime[0];
                arrivaltime1[0].value = gettime[1];
                var container = document.getElementById("edittimevalsFields");    
                var mem = "";                
                for (var i = 1; i < timevals.length; i++) {
                    gettime = timevals[i].split("-");
                    mem = mem + '<div class="form-group row"><div class="col-sm-4 mb-3 mb-sm-0"><input type="time" value="' + gettime[0] + '" required class="form-control editdeparturetime"></div>'
                            + '<div class="col-sm-4"><input type="time" value="' + gettime[1] + '" required class="form-control editarrivaltime"></div>'
                            + '<div class="col-sm-4"><a href="#" onclick="removeTimeval(this)" class="btn btn-danger btn-sm" title="Remove">Remove</a></div></div>'           
                }
                container.innerHTML = mem;
                document.getElementById("sEditDeparture").value = res.data[0].departure_station_id;
                document.getElementById("sEditArrival").value = res.data[0].arrival_station_id;
                document.getElementById("txtEditDescription").value = res.data[0].description;
                // Set days of weeks
                document.getElementById("dow0").checked = false;
                document.getElementById("dow1").checked = false;
                document.getElementById("dow2").checked = false;
                document.getElementById("dow3").checked = false;
                document.getElementById("dow4").checked = false;
                document.getElementById("dow5").checked = false;
                document.getElementById("dow6").checked = false;
                if (res.data[0].days_of_week.includes(0)){
                    document.getElementById("dow0").checked = true;
                }
                if (res.data[0].days_of_week.includes(1)){
                    document.getElementById("dow1").checked = true;
                }
                if (res.data[0].days_of_week.includes(2)){
                    document.getElementById("dow2").checked = true;
                }
                if (res.data[0].days_of_week.includes(3)){
                    document.getElementById("dow3").checked = true;
                }
                if (res.data[0].days_of_week.includes(4)){
                    document.getElementById("dow4").checked = true;
                }
                if (res.data[0].days_of_week.includes(5)){
                    document.getElementById("dow5").checked = true;
                }
                if (res.data[0].days_of_week.includes(6)){
                    document.getElementById("dow6").checked = true;
                }
                document.getElementById("txtId").value = id;
            }            
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });
}

$(document).on("submit", "#editForm", btnEditSubmit);
function btnEditSubmit(e) {
    e.preventDefault();
    const boat = document.getElementById("sEditBoat").value;
    const departuretime  = document.getElementsByClassName("editdeparturetime")
    const arrivaltime = document.getElementsByClassName("editarrivaltime");
    var timevals = "";
    for(var i=0; departuretime[i]; ++i){
        timevals = timevals + departuretime[i].value + '-' + arrivaltime[i].value + ',';        
    }
    timevals = timevals.substring(0, timevals.length - 1);
    const departure  = document.getElementById("sEditDeparture").value;
    const arrival = document.getElementById("sEditArrival").value;
    var dow = ""; 
    var inputElements = document.getElementsByClassName('checkbox-dow-edit');
    for(var i=0; inputElements[i]; ++i){
        if(inputElements[i].checked){
            dow = dow + inputElements[i].value + ',';
        }
    }
    if (dow.length > 0){
        dow = dow.substring(0, dow.length - 1);
    } else{
        alert("Please choose days of week.");
        return;
    }
    const des  = document.getElementById("txtEditDescription").value;
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "PUT",
        url: host_url +"/schedule/update_schedule",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            boat_id: boat,
            timevals: timevals,
            departure_station_id: departure,
            arrival_station_id: arrival,
            days_of_week: dow,
            description: des
        }
    })
        .then(function (res) {
            alert("Update successfully.")
            location.reload();
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

    $("#editForm").validate(
        { submitHandler: function(form) {} }
    );
}

$(document).ready(function() {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url +"/schedule/initial_new_schedule",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        }
        })
        .then(function (res) {
            var boatlst = res.data.boat_list.data;
            var content = '<option value="" selected>Choose Boat</option>';
            for (var i = 0; i < boatlst.length; i++) {
                if (boatlst[i].active == true){
                    content += ` 
                    <option value="${boatlst[i].id}">${boatlst[i].name}</option>                        
                    `;
                }
            }
            document.getElementById("sBoat").innerHTML = content;
            document.getElementById("sEditBoat").innerHTML = content;
            var stationlst = res.data.station_list.data;
            var content = '<option value="" selected>Choose Station</option>';
            for (var i = 0; i < stationlst.length; i++) {
                if (stationlst[i].active == true){
                    content += ` 
                    <option value="${stationlst[i].id}">${stationlst[i].station_name} - ${stationlst[i].location.loc_name}</option>                        
                    `;
                }                 
            }
            document.getElementById("sDeparture").innerHTML = content;
            document.getElementById("sArrival").innerHTML = content;
            document.getElementById("sEditDeparture").innerHTML = content;
            document.getElementById("sEditArrival").innerHTML = content;
            var lst = res.data.schedule_list.data;
                    var content = "";
                    for (var i = 0; i < lst.length; i++) {
                        var timelst = lst[i].timevals.replaceAll(",","<br>");
                        var dow = lst[i].days_of_week.replace("0","Sunday");
                        dow = dow.replace("1", "Monday");
                        dow = dow.replace("2", "Tuesday");
                        dow = dow.replace("3", "Wednesday");
                        dow = dow.replace("4", "Thursday");
                        dow = dow.replace("5", "Friday");
                        dow = dow.replace("6", "Saturday");
                        dow = dow.replaceAll(",","<br>");
                        if (lst[i].active == true){
                            content += ` 
                            <tr>
                                <td>${lst[i].boat.name}</td>
                                <td>${timelst}</td>
                                <td>${lst[i].departure_station.station_name}</td>
                                <td>${lst[i].arrival_station.station_name}</td>
                                <td>${dow}</td>
                                <td>${lst[i].description}</td>
                                <td><label class="switch"><input type="checkbox" onchange="changeActive(this, ${lst[i].id})" checked><span class="slider round"></span></label></td>
                                <td>
                                    <a href="#" onclick="setEditData(${lst[i].id})" class="btn btn-success btn-circle btn-sm" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fas fa-arrow-alt-circle-right"></i></a>
                                </td>
                            </tr>
                            `;
                        } else {
                            content += ` 
                            <tr>
                            <td>${lst[i].boat.name}</td>
                            <td>${timelst}</td>
                            <td>${lst[i].departure_station.station_name}</td>
                            <td>${lst[i].arrival_station.station_name}</td>
                            <td>${dow}</td>
                            <td>${lst[i].description}</td>
                                <td><label class="switch"><input type="checkbox" onchange="changeActive(this, ${lst[i].id})" ><span class="slider round"></span></label></td>
                                <td>
                                    <a href="#" onclick="setEditData(${lst[i].id})" class="btn btn-success btn-circle btn-sm" title ="Edit" data-toggle="modal" data-target="#editModal"><i class="fas fa-arrow-alt-circle-right"></i></a>
                                </td>
                            </tr>
                            `;
                        }
                        
                    }
                    document.getElementById("scheList").innerHTML = content;
        })
        .catch(function (error) {
            console.log(error);
            alert("Maybe your session is expired. You should login again.");
        });
});

function addTimeval(){
    var container = document.getElementById("timevalsFields");    
    var mem = '<div class="form-group row"><div class="col-sm-4 mb-3 mb-sm-0"><input type="time" required class="form-control departuretime"></div>'
            + '<div class="col-sm-4"><input type="time" required class="form-control arrivaltime"></div>'
            + '<div class="col-sm-4"><a href="#" onclick="removeTimeval(this)" class="btn btn-danger btn-sm" title="Remove">Remove</a></div></div>'
    container.innerHTML = container.innerHTML + mem;
}

function removeTimeval(e){
    e.parentElement.parentElement.remove(); 
}

function addEditTimeval(){
    var container = document.getElementById("timevalsFields");    
    var mem = '<div class="form-group row"><div class="col-sm-4 mb-3 mb-sm-0"><input type="time" required class="form-control editdeparturetime"></div>'
            + '<div class="col-sm-4"><input type="time" required class="form-control editarrivaltime"></div>'
            + '<div class="col-sm-4"><a href="#" onclick="removeTimeval(this)" class="btn btn-danger btn-sm" title="Remove">Remove</a></div></div>'
    container.innerHTML = container.innerHTML + mem;
}

