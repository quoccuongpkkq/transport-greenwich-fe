function logout() {
    localStorage.clear();
    window.location.assign("index.html");
}

$(document).ready(function() {
    var token = localStorage.getItem("token");
    var name = localStorage.getItem("username");
    if (token != "" && token != "undefined" && token != null){
        document.getElementById("user_name").innerHTML = name;
    } else{
        window.location.assign("login.html");
    }
        
});

