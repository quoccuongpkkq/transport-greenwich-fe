$(document).on("submit", "#createForm", btnCreateSubmit);
function btnCreateSubmit(e) {
    e.preventDefault();
    const name = document.getElementById("txtLocationName").value;
    const code = document.getElementById("txtLocationCode").value;
    const des  = document.getElementById("txtDescription").value;
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "POST",
        url: host_url +"/location",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            loc_name: name,
            loc_code: code,
            description: des
        }
    })
        .then(function (res) {
            alert("Create successfully.")
            location.reload();
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

    $("#createForm").validate(
        { submitHandler: function(form) {} }
    );
}

function changeActive(obj, id) {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "PUT",
        url: host_url +"/location",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            id: id,
            active: obj.checked
        }
    })
        .then(function (res) {
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });
}

function setEditData(id) {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url +"/searchlocation",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        params: {
            id: id
        }
    })
        .then(function (res) {
            if (res.data.length > 0){
                document.getElementById("txtEditLocationName").value = res.data[0].loc_name;
                document.getElementById("txtEditLocationCode").value = res.data[0].loc_code;
                document.getElementById("txtEditDescription").value = res.data[0].description;
                document.getElementById("txtId").value = id;
            }            
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });
}

$(document).on("submit", "#editForm", btnEditSubmit);
function btnEditSubmit(e) {
    e.preventDefault();
    const name = document.getElementById("txtEditLocationName").value;
    const code = document.getElementById("txtEditLocationCode").value;
    const des  = document.getElementById("txtEditDescription").value;
    const id = document.getElementById("txtId").value;
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "PUT",
        url: host_url +"/location",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        },
        data: {
            id: id,
            loc_name: name,
            loc_code: code,
            description: des
        }
    })
        .then(function (res) {
            alert("Update successfully.")
            location.reload();
        })
        .catch(function (error) {
            console.log(error);
            alert("Error: " + error);
        });

    $("#editForm").validate(
        { submitHandler: function(form) {} }
    );
}

$(document).ready(function() {
    var token = localStorage.getItem("token");
    var host_url = localStorage.getItem("host_url");
    axios({
        method: "GET",
        url: host_url +"/location",
        headers: {
            'Authorization': "Bearer " + token,
            'Accept': 'application/json'
        }
    })
        .then(function (res) {
            var lst = res.data.data;
            var content = "";
            for (var i = 0; i < lst.length; i++) {
                if (lst[i].active == true){
                    content += ` 
                    <tr>
                        <td>${lst[i].loc_code}</td>
                        <td>${lst[i].loc_name}</td>
                        <td>${lst[i].description}</td>
                        <td><label class="switch"><input type="checkbox" onchange="changeActive(this, ${lst[i].id})" checked><span class="slider round"></span></label></td>
                        <td>
                            <a href="#" onclick="setEditData(${lst[i].id})" class="btn btn-success btn-circle btn-sm" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fas fa-arrow-alt-circle-right"></i></a>
                        </td>
                    </tr>
                    `;
                } else {
                    content += ` 
                    <tr>
                        <td>${lst[i].loc_code}</td>
                        <td>${lst[i].loc_name}</td>
                        <td>${lst[i].description}</td>
                        <td><label class="switch"><input type="checkbox" onchange="changeActive(this, ${lst[i].id})" ><span class="slider round"></span></label></td>
                        <td>
                            <a href="#" onclick="setEditData(${lst[i].id})" class="btn btn-success btn-circle btn-sm" title="Edit" data-toggle="modal" data-target="#editModal"><i class="fas fa-arrow-alt-circle-right"></i></a>
                        </td>
                    </tr>
                    `;
                }
                
            }
            document.getElementById("locList").innerHTML = content;
        })
        .catch(function (error) {
            console.log(error);
            alert("Maybe your session is expired. You should login again.");
        });
});
